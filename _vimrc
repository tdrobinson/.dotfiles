set nocompatible
filetype off

"initialise vundle
set runtimepath+=~/.vim/bundle/Vundle.vim

"vundle packages
call vundle#begin()

Bundle 'VundleVim/Vundle.vim'

Bundle 'scrooloose/nerdtree'
Bundle 'kien/ctrlp.vim' 

Bundle 'molokai'

call vundle#end()

"basic setup
filetype plugin indent on
syntax enable
set encoding=utf-8
set showcmd

"backup/swap file setup
set nobackup
set nowritebackup
set noswapfile
set viminfo=""

"whitespace setup
set nowrap
set tabstop=4 shiftwidth=4 softtabstop=4
set expandtab
set backspace=indent,eol,start

"search setup
set hlsearch
set incsearch
set ignorecase
set smartcase

"ui setup
set number
set t_Co=256
set mouse=a
set background=dark
colorscheme molokai

"nerdtree setup
map <leader>e :NERDTreeToggle<cr>

" ctrl-p setup
let g:ctrlp_map = ';'
let g:ctrlp_cmd = 'CtrlPBuffer'

" vim-latex setup
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"
let g:Tex_CompileRule_pdf = "xelatex -interaction=nonstopmode $*"
let g:Tex_DefaultTargetFormat = "pdf"
